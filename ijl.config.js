const pkg = require('./package')

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
      },
      
  },
  navigations: {
    "bushou": "/bushou",
    "link.bushou.home": "/bushou",
    "link.bushou.radicals": "/bushou/radicals",
    "link.bushou.learn": "/bushou/learn",
  },
  config: {
    "bushou.api": "/api",
  },
};
