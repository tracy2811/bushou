import React, { StrictMode } from "react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./__data__/store";

import Dashboard from "./pages/dashboard";
//import "./app.css";

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <StrictMode>
        <Dashboard />
      </StrictMode>
    </BrowserRouter>
  </Provider>
);

export default App;
