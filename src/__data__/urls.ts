import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("bushou");

export const baseUrl = navigations["bushou"];

export const URLs = {
  root: {
    url: navigations["bushou"],
  },
  home: {
    url: navigations["link.bushou.home"],
  },
  learn: {
    url: navigations["link.bushou.learn"],
  },
  radicals: {
    url: navigations["link.bushou.radicals"],
  },
};
