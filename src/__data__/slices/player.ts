import { createSlice, createSelector } from "@reduxjs/toolkit";
import { rootSelector } from "../root-selector";

const RATES = [0.5, 1.0, 1.5, 2.0];

const { reducer, actions } = createSlice({
  name: "player",
  initialState: {
    playing: false,
    rate: 1.0,
    currentTime: 0.0,
    episode: undefined,
  },
  reducers: {
    setPlaying(state) {
      state.playing = !state.playing;
    },
    setRate(state) {
      const currentIndex = RATES.findIndex((r) => r == state.rate);
      if (currentIndex >= 0) {
        const nextIndex = (currentIndex + 1) % RATES.length;
        state.rate = RATES[nextIndex];
      }
    },
    setCurrentTime(state, { payload }) {
      state.currentTime = payload;
    },
    setEpisode(state, { payload }) {
      state.episode = payload;
      state.playing = true;
    },
  },
});

const playerRootSelector = createSelector(
  rootSelector,
  (state) => state.player
);

const selector = {
  playing: createSelector(playerRootSelector, (state) => state.playing),
  rate: createSelector(playerRootSelector, (state) => state.rate),
  currentTime: createSelector(playerRootSelector, (state) => state.currentTime),
  episode: createSelector(playerRootSelector, (state) => state.episode),
};

export { reducer, actions, selector };
