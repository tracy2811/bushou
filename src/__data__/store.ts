import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { reducer as playerReducer } from "./slices/player";

const store = configureStore({
  reducer: combineReducers({ player: playerReducer }),
});

export type RootState = ReturnType<typeof store.getState>;

export default store;
