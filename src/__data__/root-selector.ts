import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "./store";
export const rootSelector = createSelector<RootState, RootState, RootState>(
  (state) => state,
  (state) => state
);
