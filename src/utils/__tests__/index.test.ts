import { getFormatedDate } from "../";
import { describe, it, expect } from "@jest/globals";

describe("Test Utils", () => {
  it("Test getFormatedDate", () => {
    expect(getFormatedDate(123)).toBe("1/1/1970");
  });
});
