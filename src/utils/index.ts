export const groupBy = (arr: Array<any>, key: string): Array<any> =>
  arr.reduce((acc, cur) => {
    acc[cur[key]] ? acc[cur[key]].push(cur) : (acc[cur[key]] = [cur]);
    return acc;
  }, {});

export const getFormatedDuration = (second: number): string => {
  const x = Math.round(second);
  const sec = x % 60;
  const min = (x - sec) / 60;
  return `${min.toString().padStart(2, "0")}:${sec
    .toString()
    .padStart(2, "0")}`;
};

export const getMinutes = (second: number): number => Math.round(second / 60);

export const getExcerpt = (text: string): string =>
  text.length > 60 ? `${text.slice(0, 60)}... →` : `${text} →`;

export const getFormatedDate = (milliseconds: number): string =>
  new Date(milliseconds).toLocaleDateString("en-US");
