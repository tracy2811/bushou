import React from "react";
import { Typography, Container, Grid } from "@material-ui/core";
import { Nav } from "../../components";
import { useTranslation } from "react-i18next";

const Home = () => {
  const { t, i18n } = useTranslation();

  return (
    <>
      <Nav name="home" />
      <Container maxWidth="lg">
        <Grid container spacing={3} alignItems="center">
          <Grid container item xs={12} md={6} lg={4}>
            <Typography variant="h1">
              {t("bushou.home.page.proverb.ori.part1")
                .split("")
                .map((t) => (
                  <>
                    {t}
                    <br />
                  </>
                ))}
            </Typography>
            <Typography
              variant="h1"
              dangerouslySetInnerHTML={{
                __html: t("bushou.home.page.proverb.ori.part2")
                  .split("")
                  .join("<br/>"),
              }}
            ></Typography>
          </Grid>
          <Grid item xs={12} md={6} lg={8}>
            <Typography variant="h3">
              {t("bushou.home.page.proverb.trans")}
            </Typography>
            <Typography variant="h3">{t("bushou.home.page.slogan")}</Typography>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Home;
