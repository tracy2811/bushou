import React, { useState } from "react";
import {
  Typography,
  Grid,
  Card,
  CardContent,
  CardActionArea,
  Table,
  TableCell,
  TableRow,
  Collapse,
  TableBody,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";

import { Writer } from "../../../components";
import { RadicalType } from "../radical.type";
import style from "./style.css";
import { getConfigValue } from "@ijl/cli";

type RadicalProps = {
  radical: RadicalType;
};

const Radical: React.FC<RadicalProps> = ({ radical }) => {
  const [expanded, setExpanded] = useState(false);
  const { t } = useTranslation();
  return (
    <Card className={style.spacing}>
      <CardActionArea onClick={() => setExpanded(!expanded)}>
        <CardContent>
          <Grid container spacing={1}>
            <Grid item sm={4}>
              <Typography>{radical.radical.join("，")}</Typography>
            </Grid>
            <Grid item sm={4}>
              <Typography>{radical.pinyin}</Typography>
            </Grid>
            <Grid item sm={4}>
              <Typography align="right">{radical.meaning}</Typography>
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12} md="auto">
              <Writer word={radical.radical[0]} />
            </Grid>
            <Grid item xs={12} md="auto" className={style.grow}>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell className={style.tableCell}>No</TableCell>
                    <TableCell className={style.tableCell}>
                      {radical.no}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={style.tableCell}>
                      {t("bushou.radicals.page.radical.frequency")}
                    </TableCell>
                    <TableCell className={style.tableCell}>
                      {radical.frequency}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={style.tableCell}>
                      {t("bushou.radicals.page.radical.hanviet")}
                    </TableCell>
                    <TableCell className={style.tableCell}>
                      {radical.hanViet}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={style.tableCell}>
                      {t("bushou.radicals.page.radical.examples")}
                    </TableCell>
                    <TableCell className={style.tableCell}>
                      {radical.examples.join("，")}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={style.tableCell}>
                      {t("bushou.radicals.page.radical.note")}
                    </TableCell>
                    <TableCell className={style.tableCell}>
                      {radical.note}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={style.tableCell}>
                      {t("bushou.radicals.page.radical.glyph")}
                    </TableCell>
                    <TableCell className={style.tableCell}>
                      {radical.glyphOrigin.map((url, i) => (
                        <img
                          src={`${getConfigValue("bushou.api")}/images/${url}`}
                          alt={`Glyph Origin ${i}`}
                          key={url}
                        />
                      ))}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Grid>
          </Grid>
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default Radical;
