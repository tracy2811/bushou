import React, { useRef, useState, useEffect } from "react";
import { Container, Collapse, Button } from "@material-ui/core";
import { ExpandMore, ExpandLess } from "@material-ui/icons";
import Radical from "../radical/radical";
import { RadicalType } from "../radical.type";
import { useTranslation } from "react-i18next";

type RadicalGroupProps = {
  radicals: RadicalType[];
  strokeCount: number;
};

const RadicalGroup: React.FC<RadicalGroupProps> = ({
  radicals,
  strokeCount,
}) => {
  const [expanded, setExpanded] = useState(true);
  const { t } = useTranslation();

  return (
    <>
      <Button
        color="primary"
        endIcon={expanded ? <ExpandLess /> : <ExpandMore />}
        onClick={() => setExpanded(!expanded)}
      >
        {strokeCount}{" "}
        {strokeCount > 1
          ? t("bushou.radicals.page.strokes")
          : t("bushou.radicals.page.stroke")}
      </Button>
      <Container maxWidth="md">
        <Collapse in={expanded}>
          {radicals.map((r, j) => (
            <Radical radical={r} key={j} />
          ))}
        </Collapse>
      </Container>
    </>
  );
};

export default RadicalGroup;
