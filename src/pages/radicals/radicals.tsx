import React, { useState, useEffect } from "react";
import { Typography, Container } from "@material-ui/core";
import { Nav } from "../../components";
import RadicalGroup from "./radical-group/radical-group";
import axios from "axios";
import { groupBy } from "../../utils";
import { useTranslation } from "react-i18next";
import { getConfigValue } from "@ijl/cli";

const Radicals = () => {
  const [radicalGroups, setRadicalGroups] = useState([]);
  const { t } = useTranslation();

  useEffect(() => {
    (async () => {
      try {
        const { data: radicals } = await axios.get(
          `${getConfigValue("bushou.api")}/radicals`
        );
        const groups = groupBy(radicals, "strokeCount");
        const radicalGroups = Object.keys(groups).map((key) => ({
          strokeCount: key,
          radicals: groups[key],
        }));
        setRadicalGroups(radicalGroups);
      } catch (error) {
        console.error(error);
      }
    })();
  }, []);

  return (
    <>
      <Nav name="radicals"/>
      <Container maxWidth="lg">
        <Typography>{t("bushou.radicals.page.description")}</Typography>
        {radicalGroups.map((group) => (
          <RadicalGroup
            key={group.strokeCount}
            radicals={group.radicals}
            strokeCount={group.strokeCount}
          />
        ))}
      </Container>
    </>
  );
};

export default Radicals;
