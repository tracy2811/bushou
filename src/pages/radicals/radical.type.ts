export type RadicalType = {
  no: number;
  radical: string[];
  meaning: string;
  pinyin: string;
  frequency: number;
  note?: string;
  hanViet: string;
  examples?: string[];
  glyphOrigin?: string[];
  strokeCount: number;
};
