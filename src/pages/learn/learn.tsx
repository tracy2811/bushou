import React, { useState, useEffect } from "react";
import { Container } from "@material-ui/core";
import { Nav } from "../../components";
import { EpisodeDetail, EpisodeList } from "./episode";
import { EpisodeType } from "./episode/episode.type";
import axios from "axios";
import { getConfigValue } from "@ijl/cli";

const Learn = () => {
  const [episode, setEpisode] = useState<EpisodeType>();
  const [episodes, setEpisodes] = useState([]);

  useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.get(`${getConfigValue("bushou.api")}/episodes`);
        setEpisodes(data);
      } catch (error) {
        console.error(error);
      }
    })();
  }, []);

  return (
    <>
      <Nav name="learn"/>
      <Container maxWidth="lg">
        {episode ? (
          <EpisodeDetail episode={episode} setEpisode={setEpisode} />
        ) : (
          <EpisodeList setEpisode={setEpisode} episodes={episodes} />
        )}
      </Container>
    </>
  );
};

export default Learn;
