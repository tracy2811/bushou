import React from "react";
import { Typography } from "@material-ui/core";
import { useTranslation } from "react-i18next";

import { getExcerpt, getFormatedDate } from "../../../utils";
import { PlayButton } from "../../../components";
import style from "../style.css";
import { EpisodeType } from "./episode.type";

type EpisodeListProps = {
  setEpisode: (episode: EpisodeType | undefined) => void;
  episodes: EpisodeType[];
};

const EpisodeList: React.FC<EpisodeListProps> = ({ episodes, setEpisode }) => {
  const { t } = useTranslation();

  return (
    <>
      <Typography>{t("bushou.learn.page.episode.list.description")}</Typography>
      {episodes.map((e, i) => (
        <div key={i} className={style.card}>
          <img src={e.thumbnail} className={style.thumbnail} />
          <div>
            <div onClick={() => setEpisode(e)}>
              <Typography variant="h6">{e.title}</Typography>
              <Typography color="textSecondary">
                {getFormatedDate(e.pub_date_ms)}
              </Typography>
              <Typography>{getExcerpt(e.description)}</Typography>
            </div>
            <PlayButton episode={e} />
          </div>
        </div>
      ))}
    </>
  );
};

export default EpisodeList;
