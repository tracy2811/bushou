export type EpisodeType = {
  title: string;
  podcast_title: string;
  publisher: string;
  description: string;
  audio: string;
  audio_length_sec: number;
  website: string;
  thumbnail: string;
  id: string;
  pub_date_ms: number;
};