import React, { useState, useEffect } from "react";
import { Typography, Button } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import axios from "axios";

import style from "../style.css";
import { getFormatedDate } from "../../../utils";
import { DictPopover, PlayButton } from "../../../components";
import { EpisodeType } from "./episode.type";
import { useTranslation } from "react-i18next";
import { getConfigValue } from "@ijl/cli";

type EpisodeProps = {
  episode: EpisodeType;
  setEpisode: (episode: EpisodeType | undefined) => void;
};

const EpisodeDetail: React.FC<EpisodeProps> = ({ episode, setEpisode }) => {
  const [tokens, setTokens] = useState([]);
  const { t } = useTranslation();
  useEffect(() => {
    (async () => {
      try {
        const { data: tokens } = await axios.get(
          `${getConfigValue("bushou.api")}/tokenize`,
          {
            params: {
              text: episode.description,
            },
          }
        );
        setTokens(tokens);
      } catch (error) {
        console.error(error);
      }
    })();
  }, []);

  return (
    <>
      <Button
        color="primary"
        startIcon={<ArrowBack />}
        onClick={() => setEpisode(undefined)}
      >
        {t("bushou.learn.page.episode.detail.back")}
      </Button>
      <div className={style.card}>
        <img src={episode.thumbnail} className={style.thumbnail} />
        <div>
          <Typography variant="h6">{episode.title}</Typography>
          <Typography color="textSecondary">
            {getFormatedDate(episode.pub_date_ms)}
          </Typography>
          <PlayButton episode={episode} />
        </div>
      </div>
      <Typography className={style.dict}>
        {tokens.map((token, i) =>
          token.dict ? (
            <DictPopover term={token.term} dict={token.dict} key={i} />
          ) : (
            token.term
          )
        )}
      </Typography>
    </>
  );
};

export default EpisodeDetail;
