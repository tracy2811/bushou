import EpisodeList from "./episode-list";
import EpisodeDetail from "./episode-detail";
export { EpisodeDetail, EpisodeList };
