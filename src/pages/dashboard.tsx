import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

import { URLs } from "../__data__/urls";

import { Player } from "../components";
import { selector } from "../__data__/slices/player";

import Home from "./home";
import Learn from "./learn";
import Radials from "./radicals";

const Dashboard = () => {
  const episode = useSelector(selector.episode);
  return (
    <>
      <Switch>
        <Route exact path={URLs.root.url}>
          <Home />
        </Route>
        <Route exact path={URLs.radicals.url}>
          <Radials />
        </Route>
        <Route exact path={URLs.learn.url}>
          <Learn />
        </Route>
        <Route path="*">
          <Redirect to={URLs.root.url} />
        </Route>
      </Switch>
      {episode && <Player />}
    </>
  );
};

export default Dashboard;
