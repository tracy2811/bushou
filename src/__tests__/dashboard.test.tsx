import React from "react";
import { mount } from "enzyme";
import { act } from "react-dom/test-utils";
import { describe, it, expect, beforeEach } from "@jest/globals";
import { StaticRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../__data__/store";
import mockAdapter from "axios-mock-adapter";
import axios from "axios";
import radicalsMockData from "../../stubs/api/radicals/success.json";
import episodesMockData from "../../stubs/api/episodes/success.json";
import tokenizeMockData from "../../stubs/api/tokenize/success.json";

import Dashboard from "../pages/dashboard";

const multipleRequest = async (mock, responses) => {
  await act(async () => {
    await mock.onAny().reply((config) => {
      const res = responses.filter(([, url]) => config.url.includes(url));
      const [method, url, params, ...response] = res.shift();
      if (config.url.includes(url)) {
        return response;
      }
    });
  });
};

jest.mock("react-i18next", () => ({
  useTranslation: () => ({ t: (key) => key }),
}));

describe("Test dashboard", () => {
  let mockAxios;
  beforeEach(() => {
    mockAxios = new mockAdapter(axios);
  });

  it("Test Home Page", async () => {
    const app = mount(
      <Provider store={store}>
        <StaticRouter location="/bushou">
          <Dashboard />
        </StaticRouter>
      </Provider>
    );
    expect(app.find("Dashboard")).toMatchSnapshot();
  });

  it("Test Radical Page", async () => {
    const app = mount(
      <Provider store={store}>
        <StaticRouter location="/bushou/radicals">
          <Dashboard />
        </StaticRouter>
      </Provider>
    );

    // Mock request and update
    const response = [["GET", "/radicals", {}, 200, radicalsMockData]];
    await multipleRequest(mockAxios, response);
    await new Promise(setImmediate);
    app.update();

    // Collapse the first RadicalGroup
    app.find("RadicalGroup").find("button").first().simulate("click");

    // Show detail for the last radical
    app.find("button").last().simulate("click");
    // Click edit button
    app.find("button").last().simulate("click");
    // Click show/hide button
    app.find("button").last().simulate("click");
    app.find("button").last().simulate("click");

    expect(app.find("Dashboard")).toMatchSnapshot();
  });

  it("Test Learn Page - Episode List", async () => {
    const app = mount(
      <Provider store={store}>
        <StaticRouter location="/bushou/learn">
          <Dashboard />
        </StaticRouter>
      </Provider>
    );

    // Mock request and update
    const response = [["GET", "/episodes", {}, 200, episodesMockData]];
    await multipleRequest(mockAxios, response);
    await new Promise(setImmediate);
    app.update();

    // Show Player
    app.find("button").last().simulate("click");
    // Change Player rate
    app.find("button").last().simulate("click");

    expect(app.find("Dashboard")).toMatchSnapshot();
  });

  it("Test Learn Page - Episode Detail", async () => {
    const app = mount(
      <Provider store={store}>
        <StaticRouter location="/bushou/learn">
          <Dashboard />
        </StaticRouter>
      </Provider>
    );

    // Mock request and update
    const response = [
      ["GET", "/episodes", {}, 200, episodesMockData],
      [
        "GET",
        "/tokenize",
        { text: episodesMockData[episodesMockData.length - 1].description },
        200,
        tokenizeMockData,
      ],
    ];
    await multipleRequest(mockAxios, response);
    await new Promise(setImmediate);
    app.update();

    // Show the last episode detail
    app.find("h6").last().simulate("click");
    await new Promise(setImmediate);
    app.update();

    // Show DictPopover
    app.find("DictPopover").last().find("span").simulate("click");
    // Paginate
    app.find("DictPopover").last().find("button").last().simulate("click");

    expect(app.find("Dashboard")).toMatchSnapshot();
  });

  it("Test Learn Page - Episode Detail - List", async () => {
    const app = mount(
      <Provider store={store}>
        <StaticRouter location="/bushou/learn">
          <Dashboard />
        </StaticRouter>
      </Provider>
    );

    // Mock request and update
    const response = [
      ["GET", "/episodes", {}, 200, episodesMockData],
      [
        "GET",
        "/tokenize",
        { text: episodesMockData[episodesMockData.length - 1].description },
        200,
        tokenizeMockData,
      ],
    ];
    await multipleRequest(mockAxios, response);
    await new Promise(setImmediate);
    app.update();

    // Show the last episode detail
    app.find("h6").last().simulate("click");
    await new Promise(setImmediate);
    app.update();

    // Go back to Episode List
    app.find("EpisodeDetail").find("button").first().simulate("click");
    await new Promise(setImmediate);
    app.update();

    expect(app.find("Dashboard")).toMatchSnapshot();
  });
});
