import React from "react";
import { Button } from "@material-ui/core";
import { Pause, PlayArrow } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

import { actions } from "../../__data__/slices/player";
import { EpisodeType } from "../../pages/learn/episode/episode.type";
import { selector } from "../../__data__/slices/player";
import { getMinutes } from "../../utils"

const { setPlaying, setEpisode: setPlayingEpisode } = actions;

type PlayButtonProps = {
  episode: EpisodeType;
};

const PlayButton: React.FC<PlayButtonProps> = ({ episode }) => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const playing = useSelector(selector.playing);
  const playingEpisode = useSelector(selector.episode);

  const handlePlayClick = () => {
    if (episode.id == playingEpisode?.id) {
      dispatch(setPlaying());
    } else {
      dispatch(setPlayingEpisode(episode));
    }
  };

  return (
    <Button
      variant="contained"
      endIcon={
        episode.id == playingEpisode?.id && playing ? <Pause /> : <PlayArrow />
      }
      onClick={handlePlayClick}
    >
      {episode.id == playingEpisode?.id && playing
        ? t("bushou.learn.page.playing")
        : `${getMinutes(episode.audio_length_sec)} min`}
    </Button>
  );
};

export default PlayButton;
