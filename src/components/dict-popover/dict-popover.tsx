import React, { useState } from "react";
import {
  Typography,
  Card,
  CardContent,
  Popover,
  CardActions,
} from "@material-ui/core";
import { DictTermType } from "./dict-term.type";
import Pagination from "@material-ui/lab/Pagination";
import style from "./style.css";

const DictPopover: React.FC<DictTermType> = ({ term, dict }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [page, setPage] = useState(1);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleChange = (event, value) => {
    setPage(value);
  };

  const open = Boolean(anchorEl);
  const id = open ? "dic-popover" : undefined;

  return (
    <>
      <span
        aria-describedby={id}
        onClick={handleClick}
        className={open ? [style.term, style.active].join(" ") : style.term}
      >
        {term}
      </span>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Card>
          <CardContent>
            <Typography>
              {dict[page - 1].traditional} 【{dict[page - 1].simplified}】
            </Typography>
            <Typography>{dict[page - 1].pinyin}</Typography>
            {dict[page - 1].meaning.map((m, i) => (
              <Typography key={i}>
                {i + 1}. {m}
              </Typography>
            ))}
          </CardContent>
          <CardActions>
            <Pagination
              count={dict.length}
              page={page}
              onChange={handleChange}
            />
          </CardActions>
        </Card>
      </Popover>
    </>
  );
};

export default DictPopover;
