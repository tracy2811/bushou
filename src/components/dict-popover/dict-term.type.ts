
export type DictTermType = {
  term: string;
  dict: {
    simplified: string;
    traditional: string;
    meaning: string[];
    pinyin: string;
  }[];
};