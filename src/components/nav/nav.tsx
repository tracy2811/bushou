import React from "react";
import { AppBar, Toolbar, Typography, TextField } from "@material-ui/core";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { URLs } from "../../__data__/urls";
import style from "./style.css";

type NavProps = { name: string };

const Nav: React.FC<NavProps> = ({ name }) => {
  const { t } = useTranslation();

  return (
    <AppBar position="static" color="inherit" elevation={0}>
      <Toolbar className={style.toolbar}>
        <Typography
          variant="h6"
          color="inherit"
          noWrap
          className={style.toolbarTitle}
        >
          <Link to={URLs.root.url}>{t("bushou.app.name")}</Link>
        </Typography>
        <TextField placeholder={t("bushou.dictionary.page.form.term")} />
        <nav>
          <Link
            to={URLs.radicals.url}
            className={
              name == "radicals"
                ? [style.active, style.link].join(" ")
                : style.link
            }
            id="radicals-link"
          >
            {t("bushou.radicals.page.title")}
          </Link>
          <Link
            to={URLs.learn.url}
            className={
              name == "learn"
                ? [style.active, style.link].join(" ")
                : style.link
            }
          >
            {t("bushou.learn.page.title")}
          </Link>
        </nav>
      </Toolbar>
    </AppBar>
  );
};

export default Nav;
