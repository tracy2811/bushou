import React, { useEffect, useRef } from "react";
import {
  Typography,
  IconButton,
  Button,
  LinearProgress,
} from "@material-ui/core";
import { PlayArrow, SkipPrevious, SkipNext, Pause } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";

import { getFormatedDuration } from "../../utils";
import { actions, selector } from "../../__data__/slices/player";
import style from "./style.css";

const Player: React.FC = () => {
  const dispatch = useDispatch();
  const playing = useSelector(selector.playing);
  const rate = useSelector(selector.rate);
  const currentTime = useSelector(selector.currentTime);
  const episode = useSelector(selector.episode);
  const audioRef = useRef<HTMLAudioElement>();
  const TIME_STEP = 2;
  if (audioRef.current) audioRef.current.playbackRate = rate;

  const handleSkipPreviousClick = () => {
    audioRef.current.currentTime -= TIME_STEP;
  };

  const handlePlayClick = () => {
    dispatch(actions.setPlaying());
  };

  const handleSkipNextClick = () => {
    audioRef.current.currentTime += TIME_STEP;
  };

  const handleChangeRate = () => {
    dispatch(actions.setRate());
  };

  const handleTimeUpdate = () => {
    if (audioRef.current.currentTime >= audioRef.current.duration && playing) {
      dispatch(actions.setPlaying());
    }
    dispatch(actions.setCurrentTime(audioRef.current.currentTime));
  };

  useEffect(() => {
    if (playing && episode) {
      audioRef.current.play();
    } else {
      audioRef.current.pause();
    }
  }, [episode, playing]);

  return (
    <div className={style.root}>
      <audio
        ref={audioRef}
        src={episode?.audio}
        onTimeUpdate={handleTimeUpdate}
      ></audio>
      <div className={style.content}>
        <img src={episode?.thumbnail} className={style.thumbnail} />
        <div>
          <Typography>{episode?.title}</Typography>
          <Typography>{episode?.publisher}</Typography>
        </div>
      </div>
      <div className={style.action}>
        <IconButton onClick={handleSkipPreviousClick}>
          <SkipPrevious />
        </IconButton>
        <IconButton onClick={handlePlayClick}>
          {playing ? <Pause /> : <PlayArrow />}
        </IconButton>
        <IconButton onClick={handleSkipNextClick}>
          <SkipNext />
        </IconButton>
        <div className={style.progressBar}>
          <Typography>{getFormatedDuration(currentTime)}</Typography>
          <LinearProgress
            variant="determinate"
            value={
              audioRef.current
                ? (currentTime / audioRef.current.duration) * 100
                : 100
            }
            className={style.linearBar}
          />
          <Typography>
            {audioRef.current
              ? getFormatedDuration(audioRef.current.duration)
              : getFormatedDuration(episode?.audio_length_sec)}
          </Typography>
        </div>
        <Button onClick={handleChangeRate}>{rate}x</Button>
      </div>
    </div>
  );
};

export default Player;
