import React, { useRef, useState, useEffect } from "react";
import { IconButton } from "@material-ui/core";
import {
  Edit,
  Close,
  Undo,
  Visibility,
  VisibilityOff,
} from "@material-ui/icons";
import HanziWriter from "hanzi-writer";

type WriterProps = {
  word: string;
};

type WriterType = {
  cancelQuiz: Function;
  quiz: Function;
  pauseAnimation: Function;
  loopCharacterAnimation: Function;
  showOutline: Function;
  hideOutline: Function;
};

const Writer: React.FC<WriterProps> = ({ word }) => {
  const writerRef = useRef();
  const [isQuiz, setIsQuiz] = useState(false);
  const [visible, setVisible] = useState(true);
  const [writer, setWriter] = useState<WriterType>();

  const handleUndoClick = () => {
    writer.cancelQuiz();
    writer.quiz();
  };

  const handleEditClick = () => {
    if (!isQuiz) {
      writer.pauseAnimation();
      writer.quiz();
    } else {
      writer?.loopCharacterAnimation();
    }
    setIsQuiz(!isQuiz);
  };

  const handleVisibleClick = () => {
    if (!visible) {
      writer.showOutline();
    } else {
      writer.hideOutline();
    }
    setVisible(!visible);
  };

  useEffect(() => {
    const newWriter = HanziWriter.create(writerRef.current, word, {
      width: 200,
      height: 200,
    });
    newWriter.loopCharacterAnimation();
    setWriter(newWriter);
  }, []);

  return (
    <>
      <div ref={writerRef}></div>
      <div>
        <IconButton onClick={handleEditClick}>
          {isQuiz ? <Close /> : <Edit />}
        </IconButton>
        {isQuiz && (
          <>
            <IconButton onClick={handleUndoClick}>
              <Undo />
            </IconButton>
            <IconButton onClick={handleVisibleClick}>
              {visible ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </>
        )}
      </div>
    </>
  );
};

export default Writer;
