import Nav from "./nav";
import Player from "./player";
import DictPopover from "./dict-popover";
import Writer from "./writer";
import PlayButton from "./play-button";
export { Nav, Player, DictPopover, Writer, PlayButton };
