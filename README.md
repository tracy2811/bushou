# BuShou

“学如逆水行舟, 不进则退“ Keep moving forward to your high-level Chinese goal with BuShou.

## MVP 1

* Landing Page

* Radical List (with writing)

* Podcast:

    * Podcast List
    * Podcast Details (with Chinese word tokenization/dictionary)

## Design

[Figma Design](https://www.figma.com/file/msxsbdOZuuU7RNOUvjpbiU/web-bushou?node-id=0%3A3)